



    function nx_calendar (sel_date){
        var _this = this;
        this.n = 6;
        this.cur_date = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 12); // [EN] current date (XX.XX.20XX 12:00)
        this.act_month = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1, 12);                     // [EN] active month (01.XX.20XX 12:00)
        this.sel_date = null;                                                                                      // [EN] selected date (clicked)
        if (sel_date) {
            this.sel_date = new Date(sel_date.getFullYear(), sel_date.getMonth(), sel_date.getDate(), 12);
            this.act_month = new Date(sel_date.getFullYear(), sel_date.getMonth(), 1, 12); 
        }
        this.month_names = {
            '1' : Drupal.t('January'),  '4' : Drupal.t('April'), '7' : Drupal.t('July'),      '10' : Drupal.t('October'),
            '2' : Drupal.t('February'), '5' : Drupal.t('May'),   '8' : Drupal.t('August'),    '11' : Drupal.t('November'),
            '3' : Drupal.t('March'),    '6' : Drupal.t('June'),  '9' : Drupal.t('September'), '12' : Drupal.t('December')};

        this.calendar = $(_nxcal_get_html_layout(this.n));
        this.calendar_cal = $('.calendar', this.calendar);
        this.calendar_cal_days = $('tbody > tr.days > td', this.calendar_cal);
        this.calendar_cal_hd_btn_lt = $('.header .button-lt', this.calendar);
        this.calendar_cal_hd_btn_gt = $('.header .button-gt', this.calendar);
        this.calendar_cal_hd_inf_pl = $('.header .info_place', this.calendar);
        this.calendar.click(function(event){event.stopPropagation();});
        this.calendar.hide();


     // [EN] get_layout
        this.get_layout = function(){return this.calendar;}


     // [EN] user event handlers
        this.on_select_date_user_func = function(){};
        this.on_select_date = function(user_func){this.on_select_date_user_func = user_func;}


     // [EN] days on_click event
        this.calendar_cal_days.mousedown(function(){
            var new_sel_date = new Date(parseInt($(this).attr('rel')));
            if (new_sel_date.getMonth() == _this.act_month.getMonth()) {
                _this.sel_date = new_sel_date;
                _this.update_cal();
                _this.on_select_date_user_func.call(_this);
            }
        });


     // [EN] lt- and gt-buttons on_click event
        var btn_lt_gt_on_click = function(){
            if ($(this).is('.button-lt')) {_this.act_month = new Date(_this.act_month.getFullYear(), _this.act_month.getMonth()-1, 1, 12);}
            if ($(this).is('.button-gt')) {_this.act_month = new Date(_this.act_month.getFullYear(), _this.act_month.getMonth()+1, 1, 12);}
            _this.update_cal();
        };
        this.calendar_cal_hd_btn_lt.mousedown(btn_lt_gt_on_click);
        this.calendar_cal_hd_btn_gt.mousedown(btn_lt_gt_on_click);


     // [EN] show + hide
        this.show = function (){this.update_cal(); this.calendar.show();}
        this.hide = function (){this.calendar.hide();}


     // [EN] update_cal
        this.update_cal = function (){
            var matrix = this._get_matrix(this.act_month.getMonth()+1, this.act_month.getFullYear());
            for (var k = 0; k < this.calendar_cal_days.length && k < matrix.length; k++) {
                var c_el = $(this.calendar_cal_days.get(k));
                c_el.html(matrix[k]['daynum']);
                c_el.attr('rel', matrix[k]['date']);
                c_el.removeClass('prev-month next-month current selected selected-fade');
                c_el.addClass(matrix[k]['class']);
            }
            this.calendar_cal_hd_inf_pl.text(this.month_names[this.act_month.getMonth()+1]+' '+this.act_month.getFullYear());
            $('tbody > tr.days', this.calendar_cal).each(function(){
                if ($('td.next-month', $(this)).length == 7) $(this).hide();
                else $(this).show();
            })
        }


     /* -------------------------------------------- */

     
     // [EN] _nxcal_get_html_layout
        function _nxcal_get_html_layout (n){
            var output = '<div class="nx_calendar">'+
                             '<table class="header"><tr>'+
                                '<td class="button button-lt"></td>'+
                                '<td class="info_place">&nbsp;</td>'+
                                '<td class="button button-gt"></td></tr></table>'+
                             '<table class="calendar"><thead><tr>'+
                                '<td class="">Mo</td>'+
                                '<td class="">Tu</td>'+
                                '<td class="">We</td>'+
                                '<td class="">Th</td>'+
                                '<td class="">Fr</td>'+
                                '<td class="weekend">Sa</td>'+
                                '<td class="weekend">Su</td></tr></thead><tbody>';
            for (var j = 0; j < n; j++) {
                output+= '<tr class="days">';
                for (var i = 0; i < 7; i++) output+= '<td'+(i > 4 ? ' class="weekend"' : '')+'>&nbsp;</td>';
                output+= '</tr>';
            }
            return output+'</tbody></table></div>';
        }


     // [EN] _get_matrix
        this._get_matrix = function (mon_n, year_n){
            var tfunc = function (mon_n, year_n){var num = (new Date(year_n, mon_n-1, 1)).getDay(); return num == 0 ? 7 : num;}
                offset = tfunc(mon_n, year_n)-1,
                one_day = (60*60*24*1000),
                b1 = (new Date(year_n, mon_n-1, 1, 12)).valueOf(),
                b2 = (new Date(year_n, mon_n, 1, 12)).valueOf()-one_day,
                d0 = b1-(offset*one_day),
                k_not_current = false;
            for (var k = 0, matrix = [], dk = d0, k_class = ''; k < this.n*7; k++, dk+= one_day, k_class = '', k_not_current = false) {
                if (dk < b1) {k_class = 'prev-month'; k_not_current = true;}
                if (dk > b2+4000000) {k_class = 'next-month'; k_not_current = true;}
                if (Math.abs(this.cur_date.valueOf()-dk) < 4000000) k_class+= ' current';
                if (this.sel_date && Math.abs(this.sel_date.valueOf()-dk) < 4000000) {k_class+= ' selected'; if (k_not_current) k_class+= ' selected-fade';}
                matrix[k] = {'daynum':(new Date(dk)).getDate(), 'date':dk, 'class':k_class};
            }
            return matrix;
        }


     /* -----------------------------------------------------------------------
        [EN] P.S. "30.10.2011 00:00" Standard Time will change to Daylight Time
                  "31.03.2011 00:00" Daylight Time will change to Standard Time
        ----------------------------------------------------------------------- */

    }


